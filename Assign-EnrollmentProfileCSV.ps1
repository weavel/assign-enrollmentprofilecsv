param (
    [string]$CsvFilePath,
    [string]$DepEnrollmentProfileTokenName,
    [string]$DepEnrollmentProfileName
)

# Install the Microsoft.Graph module if you haven't already
# Install-Module -Name Microsoft.Graph.Beta

# Import the required modules
Import-Module Microsoft.Graph.Beta.Authentication -Force
Import-Module Microsoft.Graph.Beta.DeviceManagement -Force

# Authenticate to Microsoft Graph
#Connect-MgGraph -Scopes "DeviceManagementServiceConfig.ReadWrite.All"

# Get the enrollment profile by name
$DepEnrollmentToken = Get-MgBetaDeviceManagementDepOnboardingSetting -Filter "TokenName eq '$DepEnrollmentProfileTokenName'"

if (-not $DepEnrollmentToken) {
    Write-Error "Enrollment token '$DepEnrollmentTokenName' not found."
    Disconnect-MgGraph
    exit
}

$DepEnrollmentProfile = Get-MgBetaDeviceManagementDepOnboardingSettingEnrollmentProfile -DepOnboardingSettingId $DepEnrollmentToken.Id -Filter "$DepEnrollmentProfileName'"

if (-not $DepEnrollmentProfile) {
    Write-Error "Enrollment profile '$DepEnrollmentProfileName' not found."
    Disconnect-MgGraph
    exit
}

# Read the CSV file
$serialNumbers = Import-Csv $CsvFilePath

# Loop through each serial number and assign the enrollment profile
foreach ($serialNumber in $serialNumbers) {
    $deviceSerialNumber = $serialNumber.SerialNumber
#### HIER DOORGAAN: check of serienummer al gesynced is. filters in Get-MgBetaDeviceManagementDepOnboardingSettingImportedAppleDeviceIdentity doen het niet! Oplossing: alles binnen halen en loopen?
    # Check if the serial number exists in Intune
    $device = Get-MgDevice -Filter "serialNumber eq '$deviceSerialNumber'"
    if ($device) {
        # Assign the enrollment profile to the device
        New-MgDeviceEnrollmentConfigurationAssignment -DeviceEnrollmentConfigurationId $enrollmentProfile.id -Target $device.id
        Write-Host "Enrollment profile '$EnrollmentProfileName' assigned to device with serial number: $deviceSerialNumber"
    } else {
        Write-Host "Device with serial number $deviceSerialNumber not found in Intune."
    }
}

# Disconnect from Microsoft Graph
#Disconnect-MgGraph
